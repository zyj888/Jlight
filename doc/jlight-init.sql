/*
Navicat MySQL Data Transfer

Source Server         : Local
Source Server Version : 50538
Source Host           : localhost:3306
Source Database       : jlight

Target Server Type    : MYSQL
Target Server Version : 50538
File Encoding         : 65001

Date: 2016-12-31 20:17:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dept_id` varchar(64) NOT NULL COMMENT '部门编号',
  `name` varchar(50) NOT NULL COMMENT '部门名称',
  `parent_id` varchar(64) NOT NULL COMMENT '父部门编号',
  `is_delete` tinyint(4) NOT NULL COMMENT '是否软删除',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(255) DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('1', '1', '组织管理', '#', '0', null, '2016-12-31 17:35:51', null, '2016-12-31 17:35:54', null);

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dict_id` varchar(64) NOT NULL COMMENT '字典编号',
  `parent_id` varchar(64) NOT NULL COMMENT '父字典编号',
  `code` varchar(255) NOT NULL COMMENT '字典编码',
  `name` varchar(255) NOT NULL COMMENT '字典名称',
  `seq` tinyint(255) DEFAULT NULL COMMENT '字典序号',
  `type` varchar(255) DEFAULT NULL COMMENT '字典类型',
  `is_catagory` char(1) NOT NULL COMMENT '是否作为分类菜单  Y , N',
  `is_delete` tinyint(1) NOT NULL COMMENT '软删除标识',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新人',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('17', '1478700606660', '#', 'dict', '数据字典', '0', null, 'Y', '0', '2016-11-27 12:38:33', '', '2016-11-27 12:38:33', '', '');
INSERT INTO `sys_dict` VALUES ('23', '526508814831091244533', '1478700606660', 'sex', '测试', '0', null, 'Y', '0', '2016-12-30 14:45:24', null, '2016-12-30 14:45:24', null, null);
INSERT INTO `sys_dict` VALUES ('24', '526508814831091424245', '526508814831091244533', 'male', '男性', '0', null, 'N', '0', '2016-12-30 14:45:42', null, '2016-12-30 14:45:42', null, null);

-- ----------------------------
-- Table structure for sys_login_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_log`;
CREATE TABLE `sys_login_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login_log_id` varchar(64) NOT NULL COMMENT '登录日志编号',
  `login_account` varchar(30) NOT NULL COMMENT '登录帐号',
  `login_time` datetime NOT NULL COMMENT '登录时间',
  `login_ip` varchar(255) NOT NULL COMMENT '登录IP',
  `status` varchar(10) NOT NULL COMMENT '登录状态',
  `is_delete` int(11) NOT NULL COMMENT '是否软删除',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_by` varchar(255) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `update_by` varchar(255) DEFAULT NULL COMMENT '更新人',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_login_log
-- ----------------------------
INSERT INTO `sys_login_log` VALUES ('1', '526508814817567673211', 'admin', '2016-12-14 23:06:07', '0:0:0:0:0:0:0:1', '登录成功', '0', '2016-12-14 23:06:07', null, '2016-12-14 23:06:07', null, null);
INSERT INTO `sys_login_log` VALUES ('2', '526508814818042529571', 'admin', '2016-12-15 12:17:32', '0:0:0:0:0:0:0:1', '登录成功', '0', '2016-12-15 12:17:32', null, '2016-12-15 12:17:32', null, null);
INSERT INTO `sys_login_log` VALUES ('3', '526508814818043675972', 'admin', '2016-12-15 12:19:27', '0:0:0:0:0:0:0:1', '登录成功', '0', '2016-12-15 12:19:27', null, '2016-12-15 12:19:27', null, null);
INSERT INTO `sys_login_log` VALUES ('4', '526508814818044776691', 'admin', '2016-12-15 12:21:17', '0:0:0:0:0:0:0:1', '登录失败', '0', '2016-12-15 12:21:17', null, '2016-12-15 12:21:17', null, '帐号或者密码错误');
INSERT INTO `sys_login_log` VALUES ('5', '526508814818044823302', 'admin', '2016-12-15 12:21:22', '0:0:0:0:0:0:0:1', '登录失败', '0', '2016-12-15 12:21:22', null, '2016-12-15 12:21:22', null, '帐号或者密码错误');
INSERT INTO `sys_login_log` VALUES ('6', '526508814818044878273', 'admin', '2016-12-15 12:21:27', '0:0:0:0:0:0:0:1', '登录失败', '0', '2016-12-15 12:21:27', null, '2016-12-15 12:21:27', null, '帐号或者密码错误');
INSERT INTO `sys_login_log` VALUES ('7', '526508814818045360294', 'admin', '2016-12-15 12:22:16', '0:0:0:0:0:0:0:1', '登录失败', '0', '2016-12-15 12:22:16', null, '2016-12-15 12:22:16', null, '帐号或者密码错误');
INSERT INTO `sys_login_log` VALUES ('8', '526508814818045887691', 'admin', '2016-12-15 12:23:08', '0:0:0:0:0:0:0:1', '登录成功', '0', '2016-12-15 12:23:08', null, '2016-12-15 12:23:08', null, null);
INSERT INTO `sys_login_log` VALUES ('9', '526508814818045958602', 'admin', '2016-12-15 12:23:15', '0:0:0:0:0:0:0:1', '登录失败', '0', '2016-12-15 12:23:15', null, '2016-12-15 12:23:15', null, '帐号或者密码错误');
INSERT INTO `sys_login_log` VALUES ('10', '526508814831071174521', 'admin', '2016-12-30 14:11:57', '0:0:0:0:0:0:0:1', '登录失败', '0', '2016-12-30 14:11:57', null, '2016-12-30 14:11:57', null, '帐号或者密码错误');
INSERT INTO `sys_login_log` VALUES ('11', '526508814831071206292', 'admin', '2016-12-30 14:12:00', '0:0:0:0:0:0:0:1', '登录成功', '0', '2016-12-30 14:12:00', null, '2016-12-30 14:12:00', null, null);
INSERT INTO `sys_login_log` VALUES ('12', '526508814831074772661', 'admin', '2016-12-30 14:17:57', '0:0:0:0:0:0:0:1', '登录成功', '0', '2016-12-30 14:17:57', null, '2016-12-30 14:17:57', null, null);
INSERT INTO `sys_login_log` VALUES ('13', '526508814831075708561', 'admin', '2016-12-30 14:19:30', '0:0:0:0:0:0:0:1', '登录成功', '0', '2016-12-30 14:19:30', null, '2016-12-30 14:19:30', null, null);
INSERT INTO `sys_login_log` VALUES ('14', '526508814831080613471', 'admin', '2016-12-30 14:27:41', '0:0:0:0:0:0:0:1', '登录成功', '0', '2016-12-30 14:27:41', null, '2016-12-30 14:27:41', null, null);
INSERT INTO `sys_login_log` VALUES ('15', '526508814831090089131', 'admin', '2016-12-30 14:43:28', '0:0:0:0:0:0:0:1', '登录成功', '0', '2016-12-30 14:43:28', null, '2016-12-30 14:43:28', null, null);
INSERT INTO `sys_login_log` VALUES ('16', '526508814831090939501', 'admin', '2016-12-30 14:44:53', '0:0:0:0:0:0:0:1', '登录成功', '0', '2016-12-30 14:44:53', null, '2016-12-30 14:44:53', null, null);
INSERT INTO `sys_login_log` VALUES ('17', '526508814831096171871', 'admin', '2016-12-30 14:53:37', '0:0:0:0:0:0:0:1', '登录成功', '0', '2016-12-30 14:53:37', null, '2016-12-30 14:53:37', null, null);
INSERT INTO `sys_login_log` VALUES ('18', '526508814831856004941', 'admin', '2016-12-31 12:00:00', '0:0:0:0:0:0:0:1', '登录成功', '0', '2016-12-31 12:00:00', null, '2016-12-31 12:00:00', null, null);
INSERT INTO `sys_login_log` VALUES ('19', '526508814831858543471', 'admin', '2016-12-31 12:04:14', '0:0:0:0:0:0:0:1', '登录成功', '0', '2016-12-31 12:04:14', null, '2016-12-31 12:04:14', null, null);
INSERT INTO `sys_login_log` VALUES ('20', '526508814831860443101', 'admin', '2016-12-31 12:07:24', '0:0:0:0:0:0:0:1', '登录成功', '0', '2016-12-31 12:07:24', null, '2016-12-31 12:07:24', null, null);
INSERT INTO `sys_login_log` VALUES ('21', '526508814831861482461', 'admin', '2016-12-31 12:09:08', '0:0:0:0:0:0:0:1', '登录成功', '0', '2016-12-31 12:09:08', null, '2016-12-31 12:09:08', null, null);
INSERT INTO `sys_login_log` VALUES ('22', '526508814831862176841', 'admin', '2016-12-31 12:10:17', '127.0.0.1', '登录成功', '0', '2016-12-31 12:10:17', null, '2016-12-31 12:10:17', null, null);
INSERT INTO `sys_login_log` VALUES ('23', '526508814831863368751', 'admin', '2016-12-31 12:12:16', '0:0:0:0:0:0:0:1', '登录成功', '0', '2016-12-31 12:12:16', null, '2016-12-31 12:12:16', null, null);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `menu_id` varchar(64) NOT NULL COMMENT '菜单编号',
  `name` varchar(30) NOT NULL COMMENT '菜单名称',
  `url` varchar(30) NOT NULL COMMENT '菜单url',
  `type` int(1) NOT NULL COMMENT '菜单类型(0为一级菜单,2为二级菜单,3为按钮)',
  `icon` varchar(30) DEFAULT '' COMMENT '菜单图标',
  `is_show` int(1) NOT NULL COMMENT '是否显示(1为是，0为不是)',
  `seq` int(1) DEFAULT NULL COMMENT '菜单顺序',
  `parent_id` varchar(64) NOT NULL DEFAULT '0' COMMENT '上级菜单ID',
  `parent_name` varchar(20) DEFAULT NULL COMMENT '父菜单名称',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '软删除标识',
  `create_by` varchar(64) DEFAULT '0' COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '0' COMMENT '更新人',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `idx_res_id` (`menu_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8 COMMENT='资源表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '1', '系统管理', '', '0', 'icon-desktop', '1', '1', '#', '', '0', '0', '2016-05-16 17:10:44', '0', '2016-06-02 21:01:12', null);
INSERT INTO `sys_menu` VALUES ('2', '2', '用户管理', '/user/listPage', '0', 'icon-user', '1', '2', '1', '基础管理', '0', '0', '2016-05-06 14:34:37', '0', '2016-06-02 22:44:56', '用户管理');
INSERT INTO `sys_menu` VALUES ('3', '3', '角色管理', '/role/listPage', '0', 'icon-magnet', '1', '3', '1', '基础管理', '0', '0', '2016-05-06 14:35:24', '0', '2016-05-17 14:14:34', '角色管理');
INSERT INTO `sys_menu` VALUES ('4', '4', '菜单管理', '/menu/listPage', '0', 'icon-sitemap', '1', '4', '1', '基础管理', '0', '0', '2016-05-06 14:35:51', '0', '2016-05-15 21:23:15', '菜单管理');
INSERT INTO `sys_menu` VALUES ('19', '5', '日志管理', '', '0', 'icon-credit-card', '1', '5', '#', '', '0', null, '2016-05-22 11:17:51', null, '2016-06-02 23:09:38', null);
INSERT INTO `sys_menu` VALUES ('26', '12', '添加', '/user/add', '1', null, '1', null, '2', '用户管理', '0', null, '2016-06-02 21:00:09', null, '2016-06-02 21:00:09', '添加用户');
INSERT INTO `sys_menu` VALUES ('27', '13', '修改', '/user/edit', '1', null, '1', null, '2', '用户管理', '0', null, '2016-06-02 22:39:47', null, '2016-06-02 22:39:47', '编辑用户');
INSERT INTO `sys_menu` VALUES ('29', '14', '添加', '/role/add', '1', null, '1', null, '3', '角色管理', '0', null, '2016-06-09 00:04:16', null, '2016-06-09 00:04:16', '');
INSERT INTO `sys_menu` VALUES ('31', '16', '分配权限', '/roleRes/save', '1', null, '1', null, '3', '角色管理', '0', null, '2016-06-26 12:11:55', null, '2016-06-26 12:11:55', '保存分配的权限');
INSERT INTO `sys_menu` VALUES ('32', '17', '添加', '/menu/add', '1', null, '1', null, '4', '资源管理', '0', null, '2016-06-26 15:51:06', null, '2016-06-26 15:51:06', '添加资源');
INSERT INTO `sys_menu` VALUES ('33', '18', '修改', '/menu/edit', '1', null, '1', null, '4', '资源管理', '0', null, '2016-06-26 15:52:13', null, '2016-06-26 15:52:13', '修改资源');
INSERT INTO `sys_menu` VALUES ('34', '19', '删除', '/menu/delete', '1', null, '1', null, '4', '资源管理', '0', null, '2016-06-26 15:53:14', null, '2016-06-26 15:53:14', '删除资源');
INSERT INTO `sys_menu` VALUES ('35', '20', '删除', '/user/delete', '1', null, '1', null, '2', '用户管理', '0', null, '2016-06-26 15:56:07', null, '2016-06-26 15:56:07', '删除用户');
INSERT INTO `sys_menu` VALUES ('36', '21', '修改', '/role/edit', '1', null, '1', null, '3', '角色管理', '0', null, '2016-06-26 15:56:54', null, '2016-06-26 15:56:54', '修改角色');
INSERT INTO `sys_menu` VALUES ('37', '22', '删除', '/role/delete', '1', null, '1', null, '3', '角色管理', '0', null, '2016-06-26 15:57:14', null, '2016-06-26 15:57:14', '删除角色');
INSERT INTO `sys_menu` VALUES ('38', '23', '登录日志', '/loginLog/listPage', '0', 'icon-tasks', '1', null, '5', '监控管理', '0', null, '2016-06-26 16:07:57', null, '2016-06-26 16:07:57', '登录日志');
INSERT INTO `sys_menu` VALUES ('39', '24', '操作日志', '/webLog/list', '0', 'icon-print', '1', null, '5', '监控管理', '0', null, '2016-06-26 16:11:30', null, '2016-06-26 16:11:30', null);
INSERT INTO `sys_menu` VALUES ('41', '1478700606660', '字典管理', '/dict/list', '0', 'icon-screenshot', '1', null, '1', '系统管理', '0', null, '2016-11-09 12:42:21', null, '2016-11-09 12:42:21', '字典管理');
INSERT INTO `sys_menu` VALUES ('42', '1480978838674', '角色分配', '/user/addRoles', '1', null, '1', null, '2', '用户管理', '0', null, '2016-11-13 06:14:31', null, '2016-11-13 06:14:31', '给用户分配角色');
INSERT INTO `sys_menu` VALUES ('43', '1479219411962', '查询', '/user/list', '1', null, '1', null, '2', '用户管理', '0', null, '2016-11-15 12:49:06', null, '2016-11-15 12:49:06', null);
INSERT INTO `sys_menu` VALUES ('44', '1479219651971', '重置密码', '/user/resetPwd', '1', null, '1', null, '2', '用户管理', '0', null, '2016-11-15 12:53:06', null, '2016-11-15 12:53:06', '重置密码');
INSERT INTO `sys_menu` VALUES ('45', '1479219802326', '查询', '/role/list', '1', null, '1', null, '3', '角色管理', '0', null, '2016-11-15 12:55:37', null, '2016-11-15 12:55:37', '角色查询');
INSERT INTO `sys_menu` VALUES ('46', '1479220072000', '查询', '/loginLog/list', '1', null, '1', null, '23', '登录日志', '0', null, '2016-11-15 13:00:06', null, '2016-11-15 13:00:06', '日志查询');
INSERT INTO `sys_menu` VALUES ('47', '1479220099932', '删除', '/loginLog/delete', '1', null, '1', null, '23', '登录日志', '0', null, '2016-11-15 13:00:34', null, '2016-11-15 13:00:34', '日志删除');
INSERT INTO `sys_menu` VALUES ('48', '1479220122402', '查询', '/menu/list', '1', null, '1', null, '4', '菜单管理', '0', null, '2016-11-15 13:00:57', null, '2016-11-15 13:00:57', '菜单查询');
INSERT INTO `sys_menu` VALUES ('53', '1480136216606', '测试', '/user/test', '1', null, '1', null, '2', '用户管理', '0', null, '2016-11-26 03:29:11', null, '2016-11-26 03:29:11', null);
INSERT INTO `sys_menu` VALUES ('54', '1480224173523', '添加', '/dic/add', '1', null, '1', null, '1478700606660', '字典管理', '0', null, '2016-11-27 03:55:08', null, '2016-11-27 03:55:08', '添加');
INSERT INTO `sys_menu` VALUES ('55', '1480224199655', '修改', '/dic/edit', '1', null, '1', null, '1478700606660', '字典管理', '0', null, '2016-11-27 03:55:35', null, '2016-11-27 03:55:35', '修改');
INSERT INTO `sys_menu` VALUES ('56', '1480254855433', '删除', '/dic/delete', '1', null, '1', null, '1478700606660', '字典管理', '0', null, '2016-11-27 12:26:30', null, '2016-11-27 12:26:30', null);
INSERT INTO `sys_menu` VALUES ('59', '5265088148310982989513', '部门管理', '/dept/list', '0', 'icon-group', '1', null, '1', '系统管理', '0', null, '2016-12-30 14:57:09', null, '2016-12-30 14:57:09', null);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `role_id` varchar(64) NOT NULL COMMENT '角色编号',
  `name` varchar(32) NOT NULL COMMENT '角色名称',
  `sign` varchar(30) NOT NULL COMMENT '角色标识,程序中判断使用,如"admin"',
  `is_delete` int(1) NOT NULL DEFAULT '0' COMMENT '删除标识',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_by` varchar(64) DEFAULT '0' COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `update_by` varchar(64) DEFAULT '0' COMMENT '更新人',
  `remark` varchar(256) DEFAULT NULL COMMENT '角色描述',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_sign` (`sign`),
  KEY `idx_role_id` (`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '1', '超级管理员', 'admin', '0', '2016-04-15 11:21:00', '0', '2016-11-08 14:33:53', '0', '超级管理员');
INSERT INTO `sys_role` VALUES ('2', '5265088148310932823918', '测试', 'test', '0', '2016-12-30 14:48:48', null, '2016-12-30 14:48:48', null, null);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` varchar(64) NOT NULL COMMENT '角色编号',
  `menu_id` varchar(64) NOT NULL COMMENT '资源编号',
  `is_delete` int(1) NOT NULL DEFAULT '0' COMMENT '软删除标识',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_by` varchar(64) DEFAULT '0' COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `update_by` varchar(64) DEFAULT '0' COMMENT '更新人',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`,`role_id`,`menu_id`),
  KEY `idx_role_id` (`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=984 DEFAULT CHARSET=utf8 COMMENT='角色-资源表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('961', '1', '22', '0', '2016-11-27 12:26:44', null, '2016-11-27 12:26:44', null, null);
INSERT INTO `sys_role_menu` VALUES ('962', '1', '12', '0', '2016-11-27 12:26:44', null, '2016-11-27 12:26:44', null, null);
INSERT INTO `sys_role_menu` VALUES ('963', '1', '23', '0', '2016-11-27 12:26:44', null, '2016-11-27 12:26:44', null, null);
INSERT INTO `sys_role_menu` VALUES ('964', '1', '1479220072000', '0', '2016-11-27 12:26:44', null, '2016-11-27 12:26:44', null, null);
INSERT INTO `sys_role_menu` VALUES ('965', '1', '13', '0', '2016-11-27 12:26:44', null, '2016-11-27 12:26:44', null, null);
INSERT INTO `sys_role_menu` VALUES ('966', '1', '24', '0', '2016-11-27 12:26:44', null, '2016-11-27 12:26:44', null, null);
INSERT INTO `sys_role_menu` VALUES ('967', '1', '14', '0', '2016-11-27 12:26:44', null, '2016-11-27 12:26:44', null, null);
INSERT INTO `sys_role_menu` VALUES ('968', '1', '16', '0', '2016-11-27 12:26:44', null, '2016-11-27 12:26:44', null, null);
INSERT INTO `sys_role_menu` VALUES ('969', '1', '1480224173523', '0', '2016-11-27 12:26:44', null, '2016-11-27 12:26:44', null, null);
INSERT INTO `sys_role_menu` VALUES ('970', '1', '1479220122402', '0', '2016-11-27 12:26:44', null, '2016-11-27 12:26:44', null, null);
INSERT INTO `sys_role_menu` VALUES ('971', '1', '1479219411962', '0', '2016-11-27 12:26:44', null, '2016-11-27 12:26:44', null, null);
INSERT INTO `sys_role_menu` VALUES ('972', '1', '1478700606660', '0', '2016-11-27 12:26:44', null, '2016-11-27 12:26:44', null, null);
INSERT INTO `sys_role_menu` VALUES ('973', '1', '1', '0', '2016-11-27 12:26:44', null, '2016-11-27 12:26:44', null, null);
INSERT INTO `sys_role_menu` VALUES ('974', '1', '2', '0', '2016-11-27 12:26:44', null, '2016-11-27 12:26:44', null, null);
INSERT INTO `sys_role_menu` VALUES ('975', '1', '3', '0', '2016-11-27 12:26:44', null, '2016-11-27 12:26:44', null, null);
INSERT INTO `sys_role_menu` VALUES ('976', '1', '4', '0', '2016-11-27 12:26:44', null, '2016-11-27 12:26:44', null, null);
INSERT INTO `sys_role_menu` VALUES ('977', '1', '5', '0', '2016-11-27 12:26:44', null, '2016-11-27 12:26:44', null, null);
INSERT INTO `sys_role_menu` VALUES ('978', '1', '1480254855433', '0', '2016-11-27 12:26:44', null, '2016-11-27 12:26:44', null, null);
INSERT INTO `sys_role_menu` VALUES ('979', '1', '1480978838674', '0', '2016-11-27 12:26:44', null, '2016-11-27 12:26:44', null, null);
INSERT INTO `sys_role_menu` VALUES ('980', '1', '20', '0', '2016-11-27 12:26:44', null, '2016-11-27 12:26:44', null, null);
INSERT INTO `sys_role_menu` VALUES ('981', '1', '1479219651971', '0', '2016-11-27 12:26:44', null, '2016-11-27 12:26:44', null, null);
INSERT INTO `sys_role_menu` VALUES ('982', '1', '1479219802326', '0', '2016-11-27 12:26:44', null, '2016-11-27 12:26:44', null, null);
INSERT INTO `sys_role_menu` VALUES ('983', '1', '21', '0', '2016-11-27 12:26:44', null, '2016-11-27 12:26:44', null, null);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(64) NOT NULL COMMENT '用户编号',
  `account` varchar(30) NOT NULL COMMENT '用户帐号',
  `true_name` varchar(10) DEFAULT NULL COMMENT '真实姓名',
  `password` varchar(100) NOT NULL COMMENT '密码',
  `birth` datetime DEFAULT NULL COMMENT '出生日期',
  `sex` tinyint(1) DEFAULT NULL COMMENT '性别',
  `email` varchar(30) NOT NULL COMMENT '电子邮箱',
  `mobile` char(11) NOT NULL COMMENT '手机号码',
  `error_count` int(2) NOT NULL DEFAULT '0' COMMENT '当天登录错误次数',
  `is_lock` int(1) NOT NULL DEFAULT '0' COMMENT '用户是否锁定',
  `login_time` datetime DEFAULT NULL COMMENT '登录时间',
  `login_ip` varchar(30) DEFAULT NULL COMMENT '用户登录IP地址',
  `is_delete` tinyint(1) NOT NULL COMMENT '软删除标识',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新人',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_idx` (`user_id`),
  UNIQUE KEY `account_idx` (`account`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', '1', 'admin', 'admin', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '2016-04-15 11:23:23', null, '123456@qq.com', '13428281893', '0', '0', '2016-12-31 12:12:16', '0:0:0:0:0:0:0:1', '0', '2016-04-15 11:23:38', null, '2016-05-12 17:28:04', null, null);
INSERT INTO `sys_user` VALUES ('2', '5265088148310931446314', 'test', '测试', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', null, null, 'goqeng@gmail.com', '18218676908', '0', '0', null, null, '0', '2016-12-30 14:48:34', null, '2016-12-30 14:48:34', null, null);

-- ----------------------------
-- Table structure for sys_user_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_dept`;
CREATE TABLE `sys_user_dept` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(64) NOT NULL COMMENT ' 用户编号',
  `dept_id` varchar(64) NOT NULL COMMENT '部门编号',
  `is_delete` int(11) NOT NULL COMMENT '软删除标识(0为未删除，1为删除)',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '操作人',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user_dept
-- ----------------------------
INSERT INTO `sys_user_dept` VALUES ('1', '1', '1', '0', '1', '2016-12-31 17:47:49', '1', '2016-12-31 17:47:52', null);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_id` varchar(64) NOT NULL COMMENT '用户编号',
  `role_id` varchar(64) NOT NULL COMMENT '角色编号',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0' COMMENT '软删除标识',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_by` varchar(64) DEFAULT '0' COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `update_by` varchar(64) DEFAULT '0' COMMENT '更新人',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='用户-角色表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1', '1', '0', '2016-04-15 14:48:11', '0', '2016-04-15 14:48:13', '0', null);
INSERT INTO `sys_user_role` VALUES ('2', '5265088148310931446314', '5265088148310932823918', '0', '2016-12-30 14:48:56', null, '2016-12-30 14:48:56', null, null);

-- ----------------------------
-- Table structure for sys_web_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_web_log`;
CREATE TABLE `sys_web_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `web_log_id` varchar(255) NOT NULL COMMENT '日志编号',
  `login_account` varchar(64) NOT NULL COMMENT '登录帐号',
  `method` varchar(64) NOT NULL COMMENT '操作方法',
  `method_desc` varchar(100) DEFAULT NULL COMMENT '方法描述',
  `method_args` varchar(255) DEFAULT NULL COMMENT '方法参数',
  `operate_time` datetime NOT NULL COMMENT '操作时间',
  `operate_ip` varchar(64) NOT NULL COMMENT '操作IP',
  `status` varchar(4) DEFAULT NULL COMMENT '操作状态',
  `is_delete` int(1) NOT NULL COMMENT '是否软删除',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新人',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_web_log
-- ----------------------------
INSERT INTO `sys_web_log` VALUES ('1', '526508814817567695272', 'admin', 'list', '查询用户列表', '[com.lew.jlight.mybatis.ParamFilter@3d167019]', '2016-12-14 23:06:09', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-14 23:06:09', null, '2016-12-14 23:06:09', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('2', '526508814817567703313', 'admin', 'list', '查询角色列表', '[com.lew.jlight.mybatis.ParamFilter@a1e557a]', '2016-12-14 23:06:10', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-14 23:06:10', null, '2016-12-14 23:06:10', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('3', '526508814817567750194', 'admin', 'list', '查询登录日志列表', '[com.lew.jlight.mybatis.ParamFilter@23e84006]', '2016-12-14 23:06:15', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-14 23:06:15', null, '2016-12-14 23:06:15', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('4', '526508814831071239663', 'admin', 'list', '查询用户列表', '[com.lew.jlight.mybatis.ParamFilter@18dfe3be]', '2016-12-30 14:12:03', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:12:03', null, '2016-12-30 14:12:03', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('5', '526508814831074998992', 'admin', 'add', '添加字典', '[com.lew.jlight.web.entity.Dict@644371e8]', '2016-12-30 14:18:19', '0:0:0:0:0:0:0:1', null, '0', '2016-12-30 14:18:19', null, '2016-12-30 14:18:19', null, null);
INSERT INTO `sys_web_log` VALUES ('6', '526508814831075883192', 'admin', 'add', '添加字典', '[com.lew.jlight.web.entity.Dict@1e14bf59]', '2016-12-30 14:19:48', '0:0:0:0:0:0:0:1', null, '0', '2016-12-30 14:19:48', null, '2016-12-30 14:19:48', null, null);
INSERT INTO `sys_web_log` VALUES ('7', '526508814831076485784', 'admin', 'add', '添加字典', '[com.lew.jlight.web.entity.Dict@3c6c43d0]', '2016-12-30 14:20:48', '0:0:0:0:0:0:0:1', null, '0', '2016-12-30 14:20:48', null, '2016-12-30 14:20:48', null, null);
INSERT INTO `sys_web_log` VALUES ('8', '526508814831078275726', 'admin', 'add', '添加字典', '[com.lew.jlight.web.entity.Dict@5d3e35f]', '2016-12-30 14:23:47', '0:0:0:0:0:0:0:1', null, '0', '2016-12-30 14:23:47', null, '2016-12-30 14:23:47', null, null);
INSERT INTO `sys_web_log` VALUES ('9', '526508814831078944878', 'admin', 'add', '添加字典', '[com.lew.jlight.web.entity.Dict@32906891]', '2016-12-30 14:24:54', '0:0:0:0:0:0:0:1', null, '0', '2016-12-30 14:24:54', null, '2016-12-30 14:24:54', null, null);
INSERT INTO `sys_web_log` VALUES ('10', '5265088148310796143210', 'admin', 'add', '添加字典', '[com.lew.jlight.web.entity.Dict@8e288bf]', '2016-12-30 14:26:01', '0:0:0:0:0:0:0:1', null, '0', '2016-12-30 14:26:01', null, '2016-12-30 14:26:01', null, null);
INSERT INTO `sys_web_log` VALUES ('11', '526508814831080730542', 'admin', 'add', '添加字典', '[com.lew.jlight.web.entity.Dict@51f58abf]', '2016-12-30 14:27:53', '0:0:0:0:0:0:0:1', null, '0', '2016-12-30 14:27:53', null, '2016-12-30 14:27:53', null, null);
INSERT INTO `sys_web_log` VALUES ('12', '526508814831081245774', 'admin', 'add', '添加字典', '[com.lew.jlight.web.entity.Dict@126c9e62]', '2016-12-30 14:28:44', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:28:44', null, '2016-12-30 14:28:44', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('13', '526508814831083587146', 'admin', 'add', '添加字典', '[com.lew.jlight.web.entity.Dict@2cbf167b]', '2016-12-30 14:32:38', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:32:46', null, '2016-12-30 14:32:46', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('14', '526508814831085349358', 'admin', 'add', '添加字典', '[com.lew.jlight.web.entity.Dict@15ec90dd]', '2016-12-30 14:35:34', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:35:34', null, '2016-12-30 14:35:34', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('15', '5265088148310876961410', 'admin', 'add', '添加字典', '[com.lew.jlight.web.entity.Dict@5c9d23ae]', '2016-12-30 14:39:29', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:39:29', null, '2016-12-30 14:39:29', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('16', '526508814831090240912', 'admin', 'add', '添加字典', '[com.lew.jlight.web.entity.Dict@4798e149]', '2016-12-30 14:43:44', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:43:44', null, '2016-12-30 14:43:44', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('17', '526508814831091244422', 'admin', 'add', '添加字典', '[com.lew.jlight.web.entity.Dict@36031a35]', '2016-12-30 14:45:24', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:45:24', null, '2016-12-30 14:45:24', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('18', '526508814831091424134', 'admin', 'add', '添加字典', '[com.lew.jlight.web.entity.Dict@811d8c6]', '2016-12-30 14:45:42', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:45:42', null, '2016-12-30 14:45:42', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('19', '526508814831091531666', 'admin', 'detail', '查询字典详细', '[526508814831091244533]', '2016-12-30 14:45:53', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:45:53', null, '2016-12-30 14:45:53', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('20', '526508814831092033217', 'admin', 'detail', '查询字典详细', '[526508814831091424245]', '2016-12-30 14:46:43', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:46:43', null, '2016-12-30 14:46:43', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('21', '526508814831092077518', 'admin', 'detail', '查询字典详细', '[526508814831091424245]', '2016-12-30 14:46:47', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:46:47', null, '2016-12-30 14:46:47', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('22', '526508814831092181289', 'admin', 'detail', '查询字典详细', '[526508814831091424245]', '2016-12-30 14:46:58', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:46:58', null, '2016-12-30 14:46:58', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('23', '5265088148310929761910', 'admin', 'detail', '查询字典详细', '[526508814831091244533]', '2016-12-30 14:48:17', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:48:17', null, '2016-12-30 14:48:17', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('24', '5265088148310930153411', 'admin', 'list', '查询角色列表', '[com.lew.jlight.mybatis.ParamFilter@375bb8e9]', '2016-12-30 14:48:21', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:48:21', null, '2016-12-30 14:48:21', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('25', '5265088148310930201612', 'admin', 'list', '查询用户列表', '[com.lew.jlight.mybatis.ParamFilter@4171bfdb]', '2016-12-30 14:48:22', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:48:22', null, '2016-12-30 14:48:22', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('26', '5265088148310931445613', 'admin', 'add', '添加用户', '[com.lew.jlight.web.entity.User@48fd4fa1]', '2016-12-30 14:48:34', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:48:34', null, '2016-12-30 14:48:34', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('27', '5265088148310931583815', 'admin', 'list', '查询用户列表', '[com.lew.jlight.mybatis.ParamFilter@cc2ec6c]', '2016-12-30 14:48:35', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:48:35', null, '2016-12-30 14:48:35', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('28', '5265088148310932324516', 'admin', 'list', '查询角色列表', '[com.lew.jlight.mybatis.ParamFilter@54f4d4d]', '2016-12-30 14:48:43', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:48:43', null, '2016-12-30 14:48:43', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('29', '5265088148310932823217', 'admin', 'save', '添加角色', '[com.lew.jlight.web.entity.Role@1709ab0]', '2016-12-30 14:48:48', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:48:48', null, '2016-12-30 14:48:48', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('30', '5265088148310933145919', 'admin', 'list', '查询角色列表', '[com.lew.jlight.mybatis.ParamFilter@37ea56cb]', '2016-12-30 14:48:51', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:48:51', null, '2016-12-30 14:48:51', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('31', '5265088148310933256920', 'admin', 'list', '查询用户列表', '[com.lew.jlight.mybatis.ParamFilter@450efd54]', '2016-12-30 14:48:52', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:48:52', null, '2016-12-30 14:48:52', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('32', '5265088148310933662721', 'admin', 'add', '添加用户-角色', '[5265088148310931446314, [Ljava.lang.String;@64a619b6]', '2016-12-30 14:48:56', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:48:56', null, '2016-12-30 14:48:56', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('33', '5265088148310933950022', 'admin', 'list', '查询用户列表', '[com.lew.jlight.mybatis.ParamFilter@7473a3d5]', '2016-12-30 14:48:59', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:48:59', null, '2016-12-30 14:48:59', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('34', '5265088148310938808323', 'admin', 'add', '添加菜单', '[com.lew.jlight.web.entity.Menu@728c7b8e]', '2016-12-30 14:49:48', '0:0:0:0:0:0:0:1', '失败', '0', '2016-12-30 14:49:48', null, '2016-12-30 14:49:48', null, '父菜单不存在');
INSERT INTO `sys_web_log` VALUES ('35', '5265088148310939514824', 'admin', 'add', '添加菜单', '[com.lew.jlight.web.entity.Menu@1c89c8eb]', '2016-12-30 14:49:55', '0:0:0:0:0:0:0:1', '失败', '0', '2016-12-30 14:49:55', null, '2016-12-30 14:49:55', null, '父菜单不存在');
INSERT INTO `sys_web_log` VALUES ('36', '5265088148310941253925', 'admin', 'add', '添加菜单', '[com.lew.jlight.web.entity.Menu@28bc6b4a]', '2016-12-30 14:50:12', '0:0:0:0:0:0:0:1', '失败', '0', '2016-12-30 14:50:12', null, '2016-12-30 14:50:12', null, '父菜单不存在');
INSERT INTO `sys_web_log` VALUES ('37', '5265088148310948866726', 'admin', 'add', '添加菜单', '[com.lew.jlight.web.entity.Menu@38fef01f]', '2016-12-30 14:51:28', '0:0:0:0:0:0:0:1', '失败', '0', '2016-12-30 14:51:28', null, '2016-12-30 14:51:28', null, '父菜单不存在');
INSERT INTO `sys_web_log` VALUES ('38', '526508814831096376482', 'admin', 'add', '添加菜单', '[com.lew.jlight.web.entity.Menu@441bbd50]', '2016-12-30 14:53:57', '0:0:0:0:0:0:0:1', null, '0', '2016-12-30 14:53:57', null, '2016-12-30 14:53:57', null, null);
INSERT INTO `sys_web_log` VALUES ('39', '526508814831097037134', 'admin', 'add', '添加菜单', '[com.lew.jlight.web.entity.Menu@66468afa]', '2016-12-30 14:55:03', '0:0:0:0:0:0:0:1', null, '0', '2016-12-30 14:55:03', null, '2016-12-30 14:55:03', null, null);
INSERT INTO `sys_web_log` VALUES ('40', '526508814831097137556', 'admin', 'add', '添加菜单', '[com.lew.jlight.web.entity.Menu@645e3c77]', '2016-12-30 14:55:13', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:55:13', null, '2016-12-30 14:55:13', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('41', '526508814831097816298', 'admin', 'delete', '删除菜单', '[[526508814831097406847]]', '2016-12-30 14:56:21', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:56:21', null, '2016-12-30 14:56:21', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('42', '526508814831097896049', 'admin', 'add', '添加菜单', '[com.lew.jlight.web.entity.Menu@6c3fad38]', '2016-12-30 14:56:29', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:56:29', null, '2016-12-30 14:56:29', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('43', '5265088148310979751611', 'admin', 'delete', '删除菜单', '[[5265088148310979192310]]', '2016-12-30 14:56:37', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:56:37', null, '2016-12-30 14:56:37', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('44', '5265088148310982988812', 'admin', 'add', '添加菜单', '[com.lew.jlight.web.entity.Menu@130793ed]', '2016-12-30 14:57:09', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:57:09', null, '2016-12-30 14:57:09', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('45', '5265088148310984085914', 'admin', 'detail', '查询菜单详细', '[5265088148310982989513]', '2016-12-30 14:57:20', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:57:20', null, '2016-12-30 14:57:20', null, '操作成功');
INSERT INTO `sys_web_log` VALUES ('46', '5265088148310984470315', 'admin', 'add', '添加菜单', '[com.lew.jlight.web.entity.Menu@1bc54c3]', '2016-12-30 14:57:24', '0:0:0:0:0:0:0:1', '成功', '0', '2016-12-30 14:57:24', null, '2016-12-30 14:57:24', null, '操作成功');
