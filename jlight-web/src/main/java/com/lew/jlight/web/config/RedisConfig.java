package com.lew.jlight.web.config;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import com.lew.jlight.redis.RedisLite;

import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

import javax.annotation.Resource;

public class RedisConfig {

    @Resource
    private Environment environment;

    @Bean
    public RedisLite getJedis(){
        String host = environment.getProperty("redis.host");
        String port = environment.getProperty("redis.port");
        Preconditions.checkArgument(!Strings.isNullOrEmpty(host),"redis.host should not be null or empty");
        Preconditions.checkArgument(!Strings.isNullOrEmpty(port),"redis.port should not be null or empty");
        return new RedisLite(host,Integer.valueOf(port));
    }
}
