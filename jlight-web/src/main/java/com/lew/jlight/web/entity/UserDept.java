package com.lew.jlight.web.entity;

import com.lew.jlight.core.BaseEntity;

public class UserDept extends BaseEntity {
	private String userId;
	
	private String deptId;

	public String getUserId( ) {
		return userId;
	}

	public void setUserId( String userId ) {
		this.userId = userId;
	}

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }
}
